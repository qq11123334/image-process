function main()

    figure(1);

    for i = 1:3
        original_image = open_image(i);
        subplot(1, 3, i);
        imshow(original_image);
        title('Original Image');
    end

    mask = [
        -1, -2, -1;
        0, 0, 0;
        1, 2, 1
        ];
    figure(2);

    for i = 1:3
        subplot(2, 3, i);
        original_image = open_image(i);
        new_image = convolution(original_image, mask);
        imshow(new_image);
        title('After Sobel (horizontal)');
    end

    mask = [
        -1, 0, 1;
        -2, 0, 2;
        -1, 0, 1
        ];

    for i = 1:3
        subplot(2, 3, i + 3);
        original_image = open_image(i);
        new_image = convolution(original_image, mask);
        imshow(new_image);
        title('After Sobel (vertical)');
    end

    mask = [
        -1, -1, -1;
        -1, 8, -1;
        -1, -1, -1
        ];
    figure(3);

    for i = 1:3
        subplot(1, 3, i);
        original_image = open_image(i);
        new_image = convolution(original_image, mask);
        imshow(new_image);
        title('Laplacian of a Gaussian');
    end

end

function new_image = convolution(image, mask)
    mask_sz = size(mask);
    mask_row = mask_sz(1);
    mask_col = mask_sz(2);
    image_sz = size(image);
    height = image_sz(1);
    width = image_sz(2);
    mask_row_divide2 = (floor(mask_row / 2));
    mask_col_divide2 = (floor(mask_col / 2));

    for x = 1:mask_row_divide2

        for y = 1:mask_col
            [mask(x, y), mask(mask_row - x + 1, y)] = swap(mask(x, y), mask(mask_row - x + 1, y));
        end

    end

    for y = 1:mask_col_divide2

        for x = 1:mask_row
            [mask(x, y), mask(x, mask_col - y + 1)] = swap(mask(x, y), mask(x, mask_col - y + 1));
        end

    end

    new_image = image;

    for x = 1:height

        for y = 1:width
            sum = 0;

            for dir_x = -mask_row_divide2:mask_row_divide2

                for dir_y = -mask_col_divide2:mask_col_divide2
                    img_x = x + dir_x;
                    img_y = y + dir_y;

                    if (img_x >= 1) && (img_x <= height) && (img_y >= 1) && (img_y <= width)
                        pixel_value = image(img_x, img_y);
                        sum = sum + (pixel_value * mask(dir_x + mask_row_divide2 + 1, dir_y + mask_col_divide2 + 1));
                    end

                end

            end

            new_image(x, y) = sum;
        end

    end

end

function [a, b] = swap(A, B)
    a = B;
    b = A;
end

function image = open_image(i)
    file_path = ["image1.jpg", "image2.jpg", "image3.jpg"];
    image = imread(file_path(i));
    image = im2double(image);
end
