function main()

    height = 256;
    width = 256;
    ImageName = ["Jetplane.bmp", "Lake.bmp", "Peppers.bmp"];
    r = [2, 0.52, 1.75];

    figure(1);

    for i = 1:3
        [tmp1, tmp2] = imread(ImageName(i));
        im = ind2gray(tmp1, tmp2);

        C = 255 / (power(255, r(i)));
        subplot(2, 3, i);
        imshow(im);
        title('original image');
        im = double(im);

        for x = 1:height

            for y = 1:width
                im(x, y) = C * (im(x, y).^r(i));
            end

        end

        subplot(2, 3, i + 3);
        im = uint8(im);
        imshow(im);
        title(strcat('r=', num2str(r(i))));
    end

    figure(2);

    for i = 1:3
        [tmp1, tmp2] = imread(ImageName(i));
        im = ind2gray(tmp1, tmp2);
        subplot(4, 3, i);
        imshow(im);
        title('original image');

        Hist = uint8(zeros(height, width));

        freq = zeros(256, 1);
        probf = zeros(256, 1);
        probc = zeros(256, 1);
        cum = zeros(256, 1);
        output = zeros(256, 1);
        numofpixels = height * width;

        for x = 1:height

            for y = 1:width
                pixel = im(x, y);
                freq(pixel + 1) = freq(pixel + 1) + 1;
            end

        end

        for p = 1:256
            probf(p) = freq(p) / numofpixels;
        end

        sum = 0;

        for p = 1:256
            sum = sum + freq(p);
            cum(p) = sum;
            probc(p) = cum(p) / numofpixels;
            output(p) = round(probc(p) * 255);
        end

        for x = 1:256

            for y = 1:256
                Hist(x, y) = output(im(x, y) + 1) - 1;
            end

        end

        subplot(4, 3, i + 6);
        imshow(Hist);
        title('after Hist equation');

        subplot(4, 3, i + 3);
        imhist(im);
        title('original hist');

        subplot(4, 3, i + 9);
        imhist(Hist);
        title("hist after trans");
    end

    figure(3);

    for i = 1:3
        [tmp1, tmp2] = imread(ImageName(i));
        im = ind2gray(tmp1, tmp2);

        subplot(4, 3, i);
        imshow(im);
        title('original image');

        im = double(im);

        for j = 1:3
            subplot(4, 3, i + 3 * j);
            Lap = del2(im, j);
            imshow(Lap);
            title(strcat('after laplace mask=', int2str(j)));
        end

    end

end
