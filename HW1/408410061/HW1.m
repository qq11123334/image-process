function main()

    ImageName = ["Lena.bmp", "Peppers.bmp"];
    height = 256;
    width = 256;
    for i = 1 : 2
        figName = strcat('histogram equalization for image ', num2str(i));
        figure('Name', figName, 'NumberTitle', 'off');
        [tmp1, tmp2] = imread(ImageName(i));
        img = ind2gray(tmp1, tmp2);
        new_img = hist_eq(img);
        subplot(2, 2, 1);
        imshow(img);
        title('original image');

        subplot(2, 2, 2);
        imshow(new_img);
        title('after hist equation');

        subplot(2, 2, 3);
        [x, y] = draw(img);
        plot(x, y);
        title('original hist');

        subplot(2, 2, 4);
        [x, y] = draw(new_img);
        plot(x, y);
        title("hist after trans");
    end

    for i = 1 : 2
        figName = strcat('Local histogram equalization for image ', num2str(i));
        figure('Name', figName, 'NumberTitle', 'off');
        [tmp1, tmp2] = imread(ImageName(i));
        img = ind2gray(tmp1, tmp2);
        new_img = local_hist_eq(img);
        subplot(2, 2, 1);
        imshow(img);
        title('original image');

        subplot(2, 2, 2);
        imshow(new_img);
        title('after local hist equation');

        subplot(2, 2, 3);
        [x, y] = draw(img);
        plot(x, y);
        title('original hist');

        subplot(2, 2, 4);
        [x, y] = draw(new_img);
        plot(x, y);
        title("hist after trans");

        figName = strcat('every histogram of block for image ', num2str(i));
        figure('Name', figName, 'NumberTitle', 'off');
        block_height = height / 4;
        block_width = width / 4;
        for i = 1 : 4
            for j = 1 : 4
                x = (i - 1) * block_height + 1;
                y = (j - 1) * block_width + 1;
                block = img(x : x + block_height - 1, y : y + block_width - 1);
                [x, y] = draw(block);
                subplot(4, 4, (i - 1) * 4 + j);
                plot(x, y);    
            end
        end
    end
end
function new_img = local_hist_eq(img)
    new_img = img;
    sz = size(img);
    height = sz(1);
    width = sz(2);
    block_height = height / 4;
    block_width = width / 4;
    for i = 1 : 4
        for j = 1 : 4
            x = (i - 1) * block_height + 1;
            y = (j - 1) * block_width + 1;
            block = img(x : x + block_height - 1, y : y + block_width - 1);
            block = hist_eq(block);
            new_img(x : x + block_height - 1, y : y + block_width - 1) = block; 
        end
    end
end
function new_img = hist_eq(img)
    new_img = img;
    sz = size(img);
    height = sz(1);
    width = sz(2);
    L = 256;
    num_pixel = height * width;

    freq = zeros(L, 1);
    pre = zeros(L, 1);
    cdf = zeros(L, 1);
    T = zeros(L, 1);

    for x = 1 : height
        for y = 1 : width
            pixel = img(x, y) + 1;
            freq(pixel) = freq(pixel) + 1;
        end
    end

    pre(1) = freq(1);
    for p = 2 : L
        pre(p) = pre(p - 1) + freq(p);
    end

    for p = 1 : L
        cdf(p) = pre(p) / num_pixel;
    end

    for p = 1 : L
        T(p) = round(cdf(p) * L);
    end

    for x = 1 : height
        for y = 1 : width
            pixel = img(x, y) + 1;
            new_img(x, y) = T(pixel) - 1;
        end
    end
end

function [index, freq] = draw(img)
    L = 256;
    sz = size(img);
    height = sz(1);
    width = sz(2);
    
    index = zeros(L, 1);
    freq = zeros(L, 1);
    for x = 1 : height
        for y = 1 : width
            pixel = img(x, y);
            freq(pixel + 1) = freq(pixel + 1) + 1;
        end
    end

    for p = 1 : L
        index(p) = p - 1;
    end
end