function main()
    n = 4;

    figure(1);

    for i = 1:n
        image = open_image(i);
        subplot(4, 4, i);
        imshow(image);
        title('original image');
    end

    r = [0.4, 0.75, 3, 1.25];

    for i = 1:n
        image = open_image(i);
        image_size = size(image);
        height = image_size(1);
        width = image_size(2);
        C = 255 / (power(255, r(i)));
        image = double(image);

        for k = 1:3

            for x = 1:height

                for y = 1:width
                    image(x, y, k) = C * (image(x, y, k).^r(i));
                end

            end

        end

        image = uint8(image);
        subplot(4, 4, i + 4);
        imshow(image);
        title('in RGB');
    end

    for i = 1:n
        image = open_image(i);
        image_size = size(image);
        height = image_size(1);
        width = image_size(2);
        C = 1 / (power(1, r(i)));
        image = im2double(image);

        for x = 1:height

            for y = 1:width
                R = image(x, y, 1);
                G = image(x, y, 2);
                B = image(x, y, 3);
                [H, S, I] = RGB2HSI(R, G, B);
                I = C * (I.^r(i));
                [R, G, B] = HSI2RGB(H, S, I);
                image(x, y, 1) = R;
                image(x, y, 2) = G;
                image(x, y, 3) = B;
            end

        end

        image = uint8(255 * image);
        subplot(4, 4, i + 8);
        imshow(image);
        title('in HSI');
    end

    rL = [1.3333, 1.17647, 0.975, 0.975];
    ra = [1.375, 1, 1.25, 1.5];
    rb = [2, 1, 1.5, 1.5];

    for i = 1:n
        image = open_image(i);
        image_size = size(image);
        height = image_size(1);
        width = image_size(2);
        image = im2double(image);

        for k = 1:3

            for x = 1:height

                for y = 1:width
                    R = image(x, y, 1);
                    G = image(x, y, 2);
                    B = image(x, y, 3);
                    [L, a, b] = RGB2Lab(R, G, B);
                    L = L * rL(i);
                    a = a * ra(i);
                    b = b * rb(i);
                    [R, G, B] = Lab2RGB(L, a, b);
                    image(x, y, 1) = R;
                    image(x, y, 2) = G;
                    image(x, y, 3) = B;
                end

            end

        end

        image = uint8(255 * image);
        subplot(4, 4, i + 12);
        imshow(image);
        title('in L*a*b');
    end

end

function [H, S, I] = RGB2HSI(R, G, B)

    if R == G && G == B
        O = 0;
    else
        O = acos(((R - G + R - B) / 2) / (((R - G).^2 + (R - B) * (G - B)).^(1/2)));
    end

    if B <= G
        H = O;
    else
        H = 2 * pi - O;
    end

    S = 1 - (3 * Min(R, G, B) / (R + G + B));
    I = (R + G + B) / 3;
end

function [R, G, B] = HSI2RGB(H, S, I)

    if H < 2 * pi / 3
        B = I * (1 - S);
        R = I * (1 + ((S * cos(H)) / cos((pi / 3) - H)));
        G = 3 * I - (R + B);
    elseif H < 4 * pi / 3
        H = H - 2 * pi / 3;
        R = I * (1 - S);
        G = I * (1 + ((S * cos(H)) / cos((pi / 3) - H)));
        B = 3 * I - (R + G);
    else
        H = H - 4 * pi / 3;
        G = I * (1 - S);
        B = I * (1 + ((S * cos(H)) / cos((pi / 3) - H)));
        R = 3 * I - (G + B);
    end

end

function [L, a, b] = RGB2Lab(R, G, B)
    Xn = 0.950456;
    Yn = 1.0;
    Zn = 1.088754;
    q = 0.008856;

    X = 0.4124564 * R + 0.3575761 * G + 0.1804375 * B;
    Y = 0.2126729 * R + 0.7151522 * G + 0.0721750 * B;
    Z = 0.0193339 * R + 0.1191920 * G + 0.9503041 * B;
    X = X / Xn;
    Y = Y / Yn;
    Z = Z / Zn;

    if Y > q
        fy = Y.^(1.0/3.0);
    else
        fy = 7.787 * Y + (16.0/116.0);
    end

    if X > q
        fx = X.^(1.0/3.0);
    else
        fx = 7.787 * X + (16.0/116.0);
    end

    if Z > q
        fz = Z.^(1.0/3.0);
    else
        fz = 7.787 * Z + (16.0/116.0);
    end

    L = max(0, 116 * fy - 16);
    a = 500 * (fx - fy);
    b = 200 * (fy - fz);
end

function [R, G, B] = Lab2RGB(L, a, b)
    Xn = 0.950456;
    Yn = 1.0;
    Zn = 1.088754;
    q = 0.008856;

    fy = (L + 16.0) / 116.0;
    fx = a / 500.0 + fy;
    fz = fy - b / 200.0;

    if fx.^3 > q
        X = fx.^3;
    else
        X = (fx - (16.0/116.0)) / 7.787;
    end

    if fy.^3 > q
        Y = fy.^3;
    else
        Y = (fy - (16.0/116.0)) / 7.787;
    end

    if fz.^3 > q
        Z = fz.^3;
    else
        Z = (fz - (16.0/116.0)) / 7.787;
    end

    X = X * Xn;
    Y = Y * Yn;
    Z = Z * Zn;

    R = 3.2404542 * X - 1.5371385 * Y - 0.4985314 * Z;
    G = -0.969266 * X + 1.8760108 * Y + 0.0415560 * Z;
    B = 0.0556434 * X - 0.2040259 * Y + 1.0572252 * Z;
end

function image = open_image(i)
    ImageName = ["aloe.jpg", "church.jpg", "house.jpg", "kitchen.jpg"];
    image = imread(ImageName(i));
end

function mi = Min(a, b, c)
    mi = min(min(a, b), c);
end
