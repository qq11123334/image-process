function main()
    ImageName = ["skeleton_orig.bmp", "blurry_moon.tif"];
    A = [2, 0.4];

    for i = 1:2
        image = open_image(i);
        mask = [
            0, -1, 0,
            -1, 4, -1,
            0, -1, 0
		];

        Lap = convolution(image, mask);
        Lap_image = convolution(image, mask) + image;

        mask = [
            0, -1, 0,
            -1, A(i) + 4, -1,
            0, -1, 0
		];

        HighBoost = convolution(image, mask);
        HighBoost_image = convolution(image, mask) + image;
        
        figName = strcat("Laplacian for ", ImageName(i));
        figure('Name', figName, 'NumberTitle', 'off');
        subplot(1, 3, 1);
        imshow(image);
        title('original image');

        subplot(1, 3, 2);
        imshow(Lap);
        title('Laplacian');

        subplot(1, 3, 3);
        imshow(Lap_image);
        title('Laplacian + original image');

        figName = strcat("Hight Boost for ", ImageName(i));
        figure('Name', figName, 'NumberTitle', 'off');

        subplot(1, 3, 1);
        imshow(image);
        title('original image');

        subplot(1, 3, 2);
        imshow(HighBoost);
        title('High Boost');

        subplot(1, 3, 3);
        imshow(HighBoost_image);
        title('High Boost + original image');
    end
end

function new_image = convolution(image, mask)
    mask_sz = size(mask);
    mask_row = mask_sz(1);
    mask_col = mask_sz(2);
    image_sz = size(image);
    height = image_sz(1);
    width = image_sz(2);
    mask_row_divide2 = (floor(mask_row / 2));
    mask_col_divide2 = (floor(mask_col / 2));

    new_image = image;

    for x = 1:height
        for y = 1:width
            sum = 0.0;
            for dir_x = -mask_row_divide2:mask_row_divide2
                for dir_y = -mask_col_divide2:mask_col_divide2
                    img_x = x + dir_x;
                    img_y = y + dir_y;
                    if (img_x >= 1) && (img_x <= height) && (img_y >= 1) && (img_y <= width)
                        pixel_value = int32(image(img_x, img_y));
                        mask_value = mask(dir_x + mask_row_divide2 + 1, dir_y + mask_col_divide2 + 1);
                        sum = sum + (pixel_value * mask_value);
                    end
                end
            end
            new_image(x, y) = uint8(sum);
        end
    end
end

function image = open_image(i)
    ImageName = ["skeleton_orig.bmp", "blurry_moon.tif"];
    if i == 1
        image = imread(ImageName(i));
        image = rgb2gray(image);
    else
        image = imread(ImageName(i));
    end
end
