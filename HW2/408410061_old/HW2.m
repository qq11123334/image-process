function main()
    n = 2;

    figure(1);

    for i = 1:n
        image = open_image(i);
        subplot(1, 2, i);
        imshow(image, [0 255]);
        title('original image');
    end

    A = [4, -3, 10];
    B = [-1, 1, -1];
    Image_title = ["Laplacian operator", "unsharp masking", "high-boost filtering"];

    for k = 1:3
        figure(k + 1);
        mask = [
            [0, B(k), 0],
            [B(k), A(k), B(k)],
            [0, B(k), 0]
            ];

        for i = 1:n
            image = open_image(i);
            sharpen_image = image;
            length = size(image);
            height = length(1);
            width = length(2);

            for x = 1:height

                for y = 1:width
                    sum = 0;

                    for dir_x = -1:1

                        for dir_y = -1:1
                            mask_x = x + dir_x;
                            mask_y = y + dir_y;

                            if (mask_x >= 1) && (mask_x <= height) && (mask_y >= 1) && (mask_y <= width)
                                level = image(mask_x, mask_y);
                                level = int32(level);
                                sum = sum + (level * mask(dir_x + 2, dir_y + 2));
                            end

                        end

                    end

                    sharpen_image(x, y) = sum;
                end

            end

            subplot(1, 2, i);
            imshow(sharpen_image);
            title(strcat(Image_title(k), " in spatial"));
        end

    end

    for k = 1:3
        figure(k + 4);

        for i = 1:n
            image = open_image(i);
            image = fft2(image);
            length = size(image);
            height = length(1);
            width = length(2);
            center_x = (height / 2.0);
            center_y = (width / 2.0);

            D0 = height * width / 100;

            for x = 1:height

                for y = 1:width
                    u = (x - center_x);
                    v = (y - center_y);
                    D = (u * u + v * v);

                    if k == 1
                        image(x, y) = (1 + (4) * pi * pi * (x * x + y * y)) * image(x, y);
                    elseif k == 2

                        if D <= D0
                            image(x, y) = image(x, y);
                        else
                            image(x, y) = 2 * image(x, y);
                        end

                    elseif k == 3

                        if D <= D0
                            image(x, y) = image(x, y);
                        else
                            image(x, y) = 5 * image(x, y);
                        end

                    end

                end

            end

            sharpen_image = ifft2(image);
            sharpen_image = uint8(sharpen_image);
            subplot(1, 2, i);
            imshow(sharpen_image);
            title(strcat(Image_title(k), " in frequency"));
        end

    end

end

function image = open_image(i)
    ImageName = ["skeleton_orig.bmp", "blurry_moon.tif"];

    if i == 1
        image = imread(ImageName(i));
        image = rgb2gray(image);
    else
        image = imread(ImageName(i));
    end

end
